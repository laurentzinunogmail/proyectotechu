//nos traemos las funciones de get, post, delete.

//hacemos referencia a nuestra  libreria io
const io = require('../Utils/io')

//Refactorizado método de getUsers del GET
function getUsersV1(req,res) {
    console.log("Invocado GET /apitechu/v1/users");
    console.log("parametro top: " + req.query.$top);
    console.log("parametro count: " + req.query.$count);
    //cargo parametros
    var top = req.query.$top;
    var count = req.query.$count;
    var objReturn={};

    //cargo usuarios
    var users = require('../usuarios2.json');
    //console.log(users);
    var cuantos = users.length;

    if (count=="true"){
      objReturn.count = cuantos;
      console.log("cuantos: " + cuantos);
    }

    objReturn.users=top?users.slice(0,top):users;
    res.send(objReturn);
  }

//Refactorizado método de deleteUsers del DELETE
function deleteUserV1(req,res) {
  console.log("Invocado DELETE /apitechu/v1/users/:id");

  console.log("La id a borrar es " + req.params.id);
   //cargo usuarios
   var users = require('../usuarios2.json');

   /*FOR NORMAL*/
    for (var i = 0; i < users.length; i++)
    {
      if (users[i].id == req.params.id)
      {
        console.log(users[i].id);
        //borramos el usuario
        users.splice(i,1);
        console.log("Usuario borrado");
        break;
      }

    }
   io.writeUserDataToFile(users);

}

//Refactorizado método de createUser del POST
function createUserV1(req,res) {
  console.log("Invocado POST /apitechu/v1/users");
  //vamos a recibir info / mandarla con cabeceras
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  };
  //cargamos los usuarios
  var users = require("../usuarios2.json");
  //agregamos el nuevo usuario
  users.push(newUser);
  console.log("Usuario añadido");

  io.writeUserDataToFile(users);

  res.send(users);
}


//identifico lo que voy a sacar fuera, que se identifica como indica a la derecha.
//sin esta linea no se podrá enlazar desde fuera
module.exports.getUsersV1 = getUsersV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.createUserV1 = createUserV1;


