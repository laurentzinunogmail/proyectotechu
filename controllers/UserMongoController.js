
//nos traemos las funciones de get, post, delete.

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
// Connection URL
//const url = 'mongodb://localhost:27017/local';
const url = 'mongodb://lauren:1laurentzi@ds129904.mlab.com:29904/proyectolaurentzi';
 
// Database Name
const dbName = 'usuariosDB';

var ObjectID = require('mongodb').ObjectID;

 function test(req,res){

// Connect to the db
    MongoClient.connect(url, function(err, db) {
        if(err) { return console.dir(err); }

        db.collection('testLau', function(err, collection) {});

        db.collection('testLau', {w:1}, function(err, collection) {});

        db.createCollection('testLau', function(err, collection) {});

        db.createCollection('testLau', {w:1}, function(err, collection) {});

    });
}


function getUsersMongoV1(req,res) {
    console.log("Invocado GET /apitechu/v1/usersMongoV1");
    console.log("parametro top: " + req.query.$top);
    console.log("parametro count: " + req.query.$count);
    //cargo parametros
    var top = req.query.$top;
    var count = req.query.$count;
    var objReturn={};

    MongoClient.connect(url, function(err, db) {

        if(err) {  console.log(err); throw err;  }
    
        data = '';
    
        db.collection(dbName).find().toArray(function(err, users){
    
            if(err) throw err;
    
            //cargo usuarios
            var cuantos = users.length;

            if (count=="true"){
            objReturn.count = cuantos;
            console.log("Total: " + count);
            }

            objReturn.users=top?users.slice(0,top):users;
            
            res.send(objReturn);
    
            db.close();
    
        });
    
      });

    
  }

  
function createUserMongoV1(req,res) {
    console.log("Invocado POST /apitechu/v1/usersMongoV1");
    //vamos a recibir info / mandarla con cabeceras
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
  
    var newUser = {
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": req.body.password
    };
    MongoClient.connect(url, function(err, db) {

        if(err) { throw err;  }
        var collection = db.collection(dbName);
        collection.insert(newUser, function(err, result) {
        if(err) { throw err; }
          db.close();
    
      });
      });
  
    res.send("200");
  }

function updateUserMongoV1(req,res) {
    console.log("Invocado PUT /apitechu/v1/updateUserMongoV1/:id");
  
    console.log("La id a actualizar es " + req.params.id);
    
    MongoClient.connect(url, function(err, db) {

            if (err) throw err;

        var collection = db.collection(dbName);
        collection.update({'_id':new ObjectID(req.params.id)}, 
                { $set: 
                    {"first_name": req.body.first_name,
                    "last_name": req.body.last_name,
                    "email": req.body.email,
                    "password": req.body.password } 
                }, function(err, result) { 
                if(err) { throw err; } 
                db.close();
                res.send(200)
            });
          });
  }
  
  function deleteUserMongoV1(req,res) {
    console.log("Invocado DELETE /apitechu/v1/deleteUserMongoV1/:id");
  
    console.log("La id a borrar es " + req.params.id);
    
    MongoClient.connect(url, function(err, db) {

            if (err) throw err;
            var myquery = {_id: new ObjectID(req.params.id)};
            db.collection(dbName).deleteOne(myquery, function(err, obj) {
              if (err) throw err;
              console.log("1 document deleted");
              db.close();
              res.send(200)
            });
          });
  }
  

module.exports.test = test;
module.exports.getUsersMongoV1 = getUsersMongoV1;
module.exports.createUserMongoV1 = createUserMongoV1;
module.exports.deleteUserMongoV1 = deleteUserMongoV1;
module.exports.updateUserMongoV1 = updateUserMongoV1;
