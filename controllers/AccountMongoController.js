
//nos traemos las funciones de get, post, delete.

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
// Connection URL
const url = 'mongodb://localhost:27017/local';
 
// Database Name
const dbName = 'AccountsDB';

var ObjectID = require('mongodb').ObjectID;


function getAccountsMongoV1(req,res) {
    console.log("Invocado GET /apitechu/v1/accountsMongoV1");
    console.log("parametro top: " + req.query.$top);
    console.log("parametro count: " + req.query.$count);
    //cargo parametros
    var top = req.query.$top;
    var count = req.query.$count;
    var objReturn={};

    MongoClient.connect(url, function(err, db) {

        if(err) {  console.log(err); throw err;  }
    
        data = '';
    
        db.collection(dbName).find().toArray(function(err, accounts){
    
            if(err) throw err;
    
            //cargo cuentas
            var cuantos = accounts.length;

            if (count=="true"){
            objReturn.count = cuantos;
            console.log("Total: " + count);
            }

            objReturn.accounts=top?accounts.slice(0,top):accounts;
            
            res.send(objReturn);
    
            db.close();
    
        });
    
      });

    
  }

  
function createAccountMongoV1(req,res) {
    console.log("Invocado POST /apitechu/v1/accountsMongoV1");
    //vamos a recibir info / mandarla con cabeceras
    console.log(req.user_id);
    console.log(req.body.iban);
    console.log(req.body.creation_date);
  
    var newAccount = {
      "user_id": req.body.user_id,
      "iban": req.body.iban,
      "creation_date": req.body.creation_date,
      "active": true
    };
    MongoClient.connect(url, function(err, db) {

        if(err) { throw err;  }
        var collection = db.collection(dbName);
        collection.insert(newAccount, function(err, result) {
        if(err) { throw err; }
          db.close();
    
      });
      });
  
    res.send("200");
  }

  
function updateAccountMongoV1(req,res) {
    console.log("Invocado PUT /apitechu/v1/updateAccountsMongoV1/:id");
  
    console.log("La id a actualizar es " + req.params.id);
    
    MongoClient.connect(url, function(err, db) {

            if (err) throw err;

        var collection = db.collection(dbName);
        collection.update({'_id':new ObjectID(req.params.id)}, 
                { $set: 
                    {"user_id": req.body.user_id,
                    "iban": req.body.iban,
                    "creation_date": req.body.creation_date,
                    "active": req.body.active
                }, function(err, result) { 
                if(err) { throw err; } 
                db.close();
                res.send(200);
            }});
          });
  }
  

function deleteAccountMongoV1(req,res) {
    console.log("Invocado DELETE /apitechu/v1/deleteAccountMongoV1/:id");
  
    console.log("La id a borrar es " + req.params.id);
    
    MongoClient.connect(url, function(err, db) {

            if (err) throw err;
            var myquery = {_id: new ObjectID(req.params.id)};
            db.collection(dbName).deleteOne(myquery, function(err, obj) {
              if (err) throw err;
              console.log("1 document deleted");
              db.close();
              res.send(200)
            });
          });
  }
  

module.exports.getAccountsMongoV1 = getAccountsMongoV1;
module.exports.createAccountMongoV1 = createAccountMongoV1;
module.exports.deleteAccountMongoV1 = deleteAccountMongoV1;
module.exports.updateAccountMongoV1 = updateAccountMongoV1;
