//librerias van en constantes. nos traemos el framework
const express = require('express');
//lo ponemos en marcha
const app = express();

//hacemos referencia a nuestra nueva libreria
const io = require('./Utils/io')
//hacemos referencia a el controlador de usuarios
const userController = require('./controllers/UserController')
const authController = require('./controllers/AuthController')
const userMongoController = require('./controllers/UserMongoController')
const accountsMongoController = require('./controllers/AccountMongoController')

//le pasamos una capa de middleware para que use el parseador JSON (para que el body se muestre bien)
app.use(express.json());

const port = process.env.PORT || 3000;
app.listen(port);

console.log("API arrancada y escuchando en el puerto " + port);


app.post('/apitechu/v1/login', authController.loginV1);

app.post('/apitechu/v1/logoutV1/:id', authController.logoutV1);



app.get('/apitechu/v1/usersMongoV1', userMongoController.getUsersMongoV1);

app.post('/apitechu/v1/usersMongoV1', userMongoController.createUserMongoV1);

app.delete('/apitechu/v1/usersMongoV1/:id', userMongoController.deleteUserMongoV1);

app.put('/apitechu/v1/usersMongoV1/:id', userMongoController.updateUserMongoV1);


app.get('/apitechu/v1/accountsMongoV1', accountsMongoController.getAccountsMongoV1);

app.post('/apitechu/v1/accountsMongoV1', accountsMongoController.createAccountMongoV1);

app.delete('/apitechu/v1/accountsMongoV1/:id', accountsMongoController.deleteAccountMongoV1);

app.put('/apitechu/v1/accountsMongoV1/:id', accountsMongoController.updateAccountMongoV1);